package cat.itb.geoguesser;

import android.widget.ProgressBar;

import androidx.lifecycle.ViewModel;

public class QuizViewModel extends ViewModel {

    QuestionModel[] questionBank = new QuestionModel[] {
            new QuestionModel(R.string.q1, R.string.q1Correct, R.string.q5Correct, R.string.q7Correct, R.string.q9Correct, R.string.q1Correct),
            new QuestionModel(R.string.q2, R.string.q10Correct, R.string.q1Correct, R.string.q2Correct, R.string.q3Correct, R.string.q2Correct),
            new QuestionModel(R.string.q3, R.string.q4Correct, R.string.q3Correct, R.string.q6Correct, R.string.q10Correct, R.string.q3Correct),
            new QuestionModel(R.string.q4, R.string.q1Correct, R.string.q6Correct, R.string.q2Correct, R.string.q4Correct, R.string.q4Correct),
            new QuestionModel(R.string.q5, R.string.q9Correct, R.string.q8Correct, R.string.q7Correct, R.string.q5Correct, R.string.q5Correct),
            new QuestionModel(R.string.q6, R.string.q1Correct, R.string.q6Correct, R.string.q10Correct, R.string.q9Correct, R.string.q6Correct),
            new QuestionModel(R.string.q7, R.string.q2Correct, R.string.q4Correct, R.string.q7Correct, R.string.q3Correct, R.string.q7Correct),
            new QuestionModel(R.string.q8, R.string.q8Correct, R.string.q9Correct, R.string.q10Correct, R.string.q1Correct, R.string.q8Correct),
            new QuestionModel(R.string.q9, R.string.q9Correct, R.string.q7Correct, R.string.q2Correct, R.string.q3Correct, R.string.q9Correct),
            new QuestionModel(R.string.q10, R.string.q4Correct, R.string.q6Correct, R.string.q10Correct, R.string.q5Correct, R.string.q10Correct),
    };

    // random entre 0 i 9
    int randomNumber = (int) (Math.random()*questionBank.length);

    int currentQuestion = 1;

    int score = 0;

    int hint = 3;

    boolean isSelected = false;


    public boolean getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean s) {
        isSelected = s;
    }

    public int getRandomQuestion() {
        return randomNumber;
    }

    public int getLenght() {
        return questionBank.length;
    }

    public int getIdQuestion(int randomNumber) {
        return questionBank[randomNumber].getId();
    }

    public int getAnswer1(int randomNumber) {
        return questionBank[randomNumber].getAnswer1();
    }

    public int getAnswer2(int randomNumber) {
        return questionBank[randomNumber].getAnswer2();
    }

    public int getAnswer3(int randomNumber) {
        return questionBank[randomNumber].getAnswer3();
    }

    public int getAnswer4(int randomNumber) {
        return questionBank[randomNumber].getAnswer4();
    }

    public int getHint () {
        return  hint;
    }

    public void decreaseHint() {
        hint--;
    }

    public int getCorrectAnswer(int randomNumber) {
        return questionBank[randomNumber].getCorrectAnswer();
    }

    public int nextQuestion() {
        if(randomNumber == questionBank.length - 1) {
            randomNumber = 0;

        } else {
            randomNumber++;
        }
        return randomNumber;
    }

    public int getCurrentQuestion() {
        return currentQuestion;
    }

    public int increaseCurrentQuestion() {
       return currentQuestion++;
    }

    public void changeScore(boolean correct) {
        if(correct) score += 10;
        if(!correct) {
            if(score > 0) score -= 5;
            else score = 0;
        }
    }

    public int getScore() {
        return score;
    }

    public void restartGeoGuesser() {
        randomNumber = (int) (Math.random()*questionBank.length);
        currentQuestion = 1;
        score = 0;
        hint = 3;
        isSelected = false;
    }
}
