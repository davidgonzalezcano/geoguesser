package cat.itb.geoguesser;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.os.CountDownTimer;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Button nextQuestionBtn, hint, answer1, answer2, answer3, answer4;
    TextView question, progressQuestions, score;
    private QuizViewModel quizViewModel;
    ProgressBar progressBar;
    MyCountDownTimer myCountDownTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        quizViewModel = new QuizViewModel();
        quizViewModel = new ViewModelProvider(this).get(QuizViewModel.class);

        progressQuestions = findViewById(R.id.progressQuestions);
        question = findViewById(R.id.question);
        answer1 = findViewById(R.id.answer1);
        answer2 = findViewById(R.id.answer2);
        answer3 = findViewById(R.id.answer3);
        answer4 = findViewById(R.id.answer4);
        nextQuestionBtn = findViewById(R.id.nextQuestionBtn);
        hint = findViewById(R.id.hint);
        score = findViewById(R.id.score);
        progressBar = findViewById(R.id.progressBar);

        answer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicAnyAnswer(answer1);
            }
        });

        answer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               clicAnyAnswer(answer2);
            }
        });

        answer3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicAnyAnswer(answer3);
            }
        });

        answer4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicAnyAnswer(answer4);
            }
        });

        nextQuestionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(quizViewModel.getCurrentQuestion() < quizViewModel.getLenght()) {

                if(quizViewModel.getCurrentQuestion() == quizViewModel.getLenght()-1) {
                    nextQuestionBtn.setText(R.string.finish);
                }

                quizViewModel.increaseCurrentQuestion();

                nextQuestion();

                quizViewModel.setIsSelected(false);

                progresBarCountDown();
            }
            else {
                eventDialog();
            }
            }
        });


        hint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(!quizViewModel.isSelected) {

                    quizViewModel.decreaseHint();

                    hint.setText("HINT " + quizViewModel.getHint());

                    nextQuestionBtn.setVisibility(View.VISIBLE);

                    quizViewModel.setIsSelected(true);

                } else {
                    hint.setText("HINT " + quizViewModel.getHint());
                }

                int questionNumber = quizViewModel.getRandomQuestion();
                String correctAnswer =getText(quizViewModel.getCorrectAnswer(questionNumber)).toString();
                revealCorrectAnswer(correctAnswer);

                if(quizViewModel.getHint() <= 0) {
                    hint.setVisibility(View.INVISIBLE);
                    hint.setText("HINT " + quizViewModel.getHint());
                }
            }
        });


        // inici de Geo Guesser
        StartGeoGuesser();

        progressBar.getProgressDrawable().setColorFilter(
                Color.rgb(255,193,7), android.graphics.PorterDuff.Mode.SRC_IN);

        if(quizViewModel.getHint() <= 0) {
            hint.setVisibility(View.INVISIBLE);
            hint.setText("HINT " + quizViewModel.getHint());
        }

        if(quizViewModel.isSelected) {
            int questionNumber = quizViewModel.getRandomQuestion();
            String correctAnswer =getText(quizViewModel.getCorrectAnswer(questionNumber)).toString();
            revealCorrectAnswer(correctAnswer);
        }

        if(quizViewModel.getCurrentQuestion() == quizViewModel.getLenght()) {
            nextQuestionBtn.setText(R.string.finish);
        }
    }


    public void StartGeoGuesser() {
        // Carregar el progrés de preguntes
        progress();

        // Carregar la pregunta amb les respostes
        assigntQuestionValue(quizViewModel.getRandomQuestion());

        hint.setText("HINT " + quizViewModel.getHint());

        score.setText("Score: " + quizViewModel.getScore() + "/100");

        progresBarCountDown();

        if(!quizViewModel.isSelected) {
            nextQuestionBtn.setVisibility(View.INVISIBLE);
        }
        else {
            nextQuestionBtn.setVisibility(View.VISIBLE);

            int questionNumber = quizViewModel.getRandomQuestion();
            String correctAnswer =getText(quizViewModel.getCorrectAnswer(questionNumber)).toString();
            revealCorrectAnswer(correctAnswer);
        }
    }


    public void progress() {
        progressQuestions.setText("Question " +
                quizViewModel.getCurrentQuestion() + " of " + quizViewModel.getLenght());
    }


    public void assigntQuestionValue(int randomQuestion) {
        question.setText(quizViewModel.getIdQuestion(randomQuestion));
        answer1.setText(quizViewModel.getAnswer1(randomQuestion));
        answer2.setText(quizViewModel.getAnswer2(randomQuestion));
        answer3.setText(quizViewModel.getAnswer3(randomQuestion));
        answer4.setText(quizViewModel.getAnswer4(randomQuestion));
    }


    public void clicAnyAnswer(Button answer) {
        //Si no s'ha fet clic a cap botó anteriorment
        if(!quizViewModel.isSelected) {
            int questionNumber = quizViewModel.getRandomQuestion();
            String correctAnswer =getText(quizViewModel.getCorrectAnswer(questionNumber)).toString();
            String userAnswer = answer.getText().toString();

            revealCorrectAnswer(correctAnswer);

            boolean increaseScore = isCorrect(correctAnswer, userAnswer);

            quizViewModel.changeScore(increaseScore);

            score.setText("Score: " + quizViewModel.getScore() + "/100");
            quizViewModel.setIsSelected(true);
        }
        nextQuestionBtn.setVisibility(View.VISIBLE);
    }


    public void revealCorrectAnswer(String correctAnswer) {
        List<Button> answers = new ArrayList<Button>();
        answers.add(answer1);
        answers.add(answer2);
        answers.add(answer3);
        answers.add(answer4);

        for(int i =0; i < 4; i++) {
            String textAnswer = answers.get(i).getText().toString();

            if(textAnswer.equals(correctAnswer)){
                answers.get(i).setBackgroundResource(R.drawable.correct);
            }
            else answers.get(i).setBackgroundResource(R.drawable.incorrect);
        }
    }


    public boolean isCorrect(String correctAnswer, String userAnswer) {
        if(userAnswer.equals(correctAnswer)) return true;
        else return false;
    }



    public void nextQuestion() {
        assigntQuestionValue(quizViewModel.nextQuestion());

        progress();

        resetRevealAnswer();

        nextQuestionBtn.setVisibility(View.INVISIBLE);

        if(quizViewModel.getHint() == 0) {
            hint.setVisibility(View.INVISIBLE);
        }
    }


    public void resetRevealAnswer() {
        answer1.setBackgroundResource(R.drawable.rounded_corners);
        answer2.setBackgroundResource(R.drawable.rounded_corners);
        answer3.setBackgroundResource(R.drawable.rounded_corners);
        answer4.setBackgroundResource(R.drawable.rounded_corners);
    }


    //Missatge al final del quiz
    public void eventDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("This is your Score!");
        builder.setMessage("Total score: " + quizViewModel.getScore() + "/100");

        builder.setPositiveButton("Restart", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                quizViewModel.restartGeoGuesser();

                StartGeoGuesser();

                resetRevealAnswer();

                hint.setVisibility(View.VISIBLE);

            }
        });
        builder.setNegativeButton("Finish", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        AlertDialog dialog = builder.create();

        dialog.show();
    }



    public void progresBarCountDown() {
        progressBar.setProgress(100);
        myCountDownTimer = new MyCountDownTimer(10000, 1000);
        myCountDownTimer.start();
    }

    public class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if(quizViewModel.isSelected) {
                progressBar.setProgress(0);
                myCountDownTimer.cancel();
            }
            else {
                int progress = (int) (millisUntilFinished/100);
                progressBar.setProgress(progress);
            }

        }

        @Override
        public void onFinish() {
            progressBar.setProgress(0);
            quizViewModel.isSelected = true;

            int questionNumber = quizViewModel.getRandomQuestion();
            String correctAnswer =getText(quizViewModel.getCorrectAnswer(questionNumber)).toString();
            revealCorrectAnswer(correctAnswer);

            nextQuestionBtn.setVisibility(View.VISIBLE);

        }
    }
}