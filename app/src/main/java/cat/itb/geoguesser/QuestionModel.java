package cat.itb.geoguesser;

public class QuestionModel {
    private int id;
    private int answer1;
    private int answer2;
    private int answer3;
    private int answer4;
    private int correctAnswer;


    public QuestionModel(int id, int answer1, int answer2, int answer3, int answer4, int correctAnswer) {
        this.id = id;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
        this.correctAnswer = correctAnswer;
    }

    public int getId() {
        return id;
    }

    public int getAnswer1() {
        return answer1;
    }

    public int getAnswer2() {
        return answer2;
    }

    public int getAnswer3() {
        return answer3;
    }

    public int getAnswer4() {
        return answer4;
    }

    public int getCorrectAnswer() {
        return correctAnswer;
    }
}
